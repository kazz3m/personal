#!/bin/bash

CREATE_KEY=$2
DEST_HOST=$1
LOCAL_HOST=$(hostname -a | sed 's/h-//')
DOMAIN=$(hostname -d)
DEST_SERVER="h-"$DEST_HOST"."$DOMAIN
DEST_PORT="2800"$DEST_HOST
LOCAL_PORT="2800"$LOCAL_HOST

if [ "$CREATE_KEY" = "true" ]; then
    openvpn --genkey --secret /etc/openvpn/$DOMAIN.key
fi

mkdir /var/log/openvpn
chown nobody:nogroup /var/log/openvpn

NEW_CONF="vpn-to-host-$DEST_HOST.conf"
cp vpn-to-host-NUM.conf $NEW_CONF
sed -i "s/DEST_HOST_ID/$DEST_HOST/g" $NEW_CONF
sed -i "s/LOCAL_HOST_ID/$LOCAL_HOST/g" $NEW_CONF
sed -i "s/DOMAIN/$DOMAIN/g" $NEW_CONF
sed -i "s/DEST_PORT/$DEST_PORT/g" $NEW_CONF
sed -i "s/LOCAL_PORT/$LOCAL_PORT/g" $NEW_CONF
sed -i "s/DEST_SERVER/$DEST_SERVER/g" $NEW_CONF
mv $NEW_CONF /etc/openvpn

NAME=$(echo $NEW_CONF | sed 's/.conf//')
/etc/init.d/openvpn start $NAME


