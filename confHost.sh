#!/bin/bash

LOCAL_HOST_ID=$(echo $(hostname -s) | sed 's/h-//')
SRV_DOMAIN=$(hostname -d)
SRV_EXT_IP=`curl --silent  xtr.net.pl`
if [ "$SRV_EXT_IP" = "" ]; then
    SRV_EXT_IP=`cat /etc/hosts | egrep -v "(127|#|:)" | grep $SRV_DOMAIN | cut -d' ' -f1`
fi
cd /opt

apt-get update
apt-get -y upgrade
apt-get -y install mc lxc nmap ntp iptraf iotop iftop htop git wget zabbix-agent openvpn libssl-dev openssl bridge-utils acl

# echo "deb http://linux-packages.getsync.com/btsync/deb btsync non-free" > /etc/apt/sources.list.d/btsync.list
# wget -q http://linux-packages.getsync.com/btsync/key.asc -O - | apt-key add -
# apt-get update && apt-get -y install btsync

adduser --home /home/deploy --shell /bin/bash --uid 1001 --gid 33 --disabled-password --gecos "" deploy
mkdir -p /var/ct3_sync/www/
chown -R deploy:deploy /var/ct3_sync/
chmod -R g+s /var/ct3_sync/
chmod -R 775 /var/ct3_sync/
setfacl -Rm d:u::rwx,g::rwx,o::rX /var/ct3_sync/


sed -i "s/10.0.3/10.30.$LOCAL_HOST_ID/g" /etc/default/lxc-net
service lxc-net restart 

wget https://opscode-omnibus-packages.s3.amazonaws.com/ubuntu/10.04/x86_64/chef_12.7.2-1_amd64.deb
dpkg -i chef_12.7.2-1_amd64.deb

mkdir VPNhelper
cd VPNhelper
git clone https://kazz3m@bitbucket.org/kazz3m/personal.git .

cat ssh_pub_keys >> /root/.ssh/authorized_keys2

chmod +x setup_VPN.sh

HOSTS_LIST=$(cat hosts-ct.list | tr '\n' ' ' | sed "s/`hostname -s`//" | sed -e "s/^[[:space:]]//" -e "s/[[:space:]]$//" -e "s/[[:space:]]{2,}//")
CREATE_SEC_KEY="false"
if [ "$LOCAL_HOST_ID" = "1" ]; then
    CREATE_SEC_KEY="true"
fi

cp /usr/share/doc/openvpn/examples/sample-scripts/bridge-* /etc/openvpn

for HOST in $HOSTS_LIST;
    do
	HOST_ID=$(echo $HOST | sed 's/h-//')
	./setup_VPN.sh $HOST_ID $CREATE_SEC_KEY;
    done

cd /

mkdir -p /data/shared
ln -s /var/lib/lxc /vservers

sed -i 's/#net.ipv4.ip_forward/net.ipv4.ip_forward/' /etc/sysctl.conf
sysctl -p 

echo "domain $(hostname -d)" >> /etc/resolv.conf

ln -s /var/ct3_sync/ /data/ct3_sync

exit 0;


